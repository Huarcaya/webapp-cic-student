import { Generic } from '../interfaces/generic';

export class Menu implements Generic {
  static readonly ADMIN_MENU: Menu[] = [
    { id: 1, name: 'Home', url: '/admin', isExternal: false },
    { id: 1, name: 'Languages', url: '/admin/languages', isExternal: false },
    { id: 1, name: 'Programs', url: '/admin/programs', isExternal: false },
  ];
  static readonly USER_MENU: Menu[] = [
    { id: 1, name: 'Profile', url: '/user/profile', isExternal: false },
    { id: 1, name: 'Settings', url: '/user/settings', isExternal: false },
  ];
  static readonly EMPLOYEE_MENU: Menu[] = [
    { id: 1, name: 'Inicio', url: '', isExternal: false },
    {
      id: 1,
      name: 'Secciones',
      url: '/workspace/classroom',
      isExternal: false,
    },
    { id: 1, name: 'Idiomas', url: '/workspace/languages', isExternal: false },
  ];
  static readonly STUDENT_MENU: Menu[] = [
    { id: 1, name: 'Home', url: '', isExternal: false },
    {
      id: 1,
      name: 'Academic Record',
      url: '/student/academic-record',
      isExternal: false,
    },
    {
      id: 1,
      name: 'Attendance',
      url: '/student/attendance',
      isExternal: false,
    },
  ];

  id!: number;
  name!: string;
  url!: string;
  isExternal!: boolean;
}
