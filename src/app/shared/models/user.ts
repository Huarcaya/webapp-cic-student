import { Generic } from '../interfaces/generic';

export class User implements Generic {
  id!: number;
  user!: string;
  username!: string;
  password!: string;
  firstName!: string;
  lastName!: string;
  email!: string;
  pidm!: number;
  roles: string[] = [];
  grant_type!: string;
}
