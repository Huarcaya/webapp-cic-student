import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Config } from 'src/app/config/config';

@Injectable({
  providedIn: 'root',
})
export class MenusService {
  private baseEndpoint = `${Config.API_URL}menus`;

  constructor(private http: HttpClient) {}

  getMenus(): Observable<any> {
    return this.http.get<any>(this.baseEndpoint);
  }
}
