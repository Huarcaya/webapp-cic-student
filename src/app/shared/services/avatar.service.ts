import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AvatarService {
  /**
   * Generate an avatar
   * https://dev.to/dcodeyt/build-a-user-profile-avatar-generator-with-javascript-436m
   * @param text Text (e.g.: 'Paco Yunque')
   * @param foregroundColor Text color (e.g.: #e9ecef)
   * @param backgroundColor Background color (e.g.: ##222e3c)
   * @returns
   */
  generateAvatar(
    text: string = '?',
    foregroundColor: string = '#d3e2f7',
    backgroundColor: string = '#3b7ddd'
  ): string {
    let textSplitted: string[] = [];
    let textToRender: string = '?';
    text = text.trim() || '?';

    textSplitted = text.length > 1 ? text.split(' ') : [text];

    if (textSplitted.length > 1) {
      textToRender =
        textSplitted[0][0].toUpperCase() + textSplitted[1][0].toUpperCase();
    } else {
      textToRender = textSplitted[0][0].toUpperCase();
    }

    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d') || new CanvasRenderingContext2D();

    canvas.width = 200;
    canvas.height = 200;

    // Draw background
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height);

    // Draw text
    context.font = 'bold 100px Assistant';
    context.fillStyle = foregroundColor;
    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.fillText(textToRender, canvas.width / 2, canvas.height / 2);

    return canvas.toDataURL('image/png');
  }
}
