import { Component, OnInit } from '@angular/core';

import { Config } from 'src/app/config/config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  title = Config.APP.name;

  constructor() {}

  ngOnInit(): void {}
}
