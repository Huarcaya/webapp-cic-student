import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { interval, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { AuthService } from 'src/app/auth/services/auth.service';
import { AvatarService } from '../../services/avatar.service';
import { Language } from '../../interfaces/language.interface';
import { ProfileService } from 'src/app/user/services/profile.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  user: User = new User();
  profile!: Language;

  readonly hoursInADay = 24;
  readonly minutesInAnHour = 60;
  readonly secondsInAMinute = 60;
  readonly milliSecondsInASecond = 1000;

  readonly minutesInMilliseconds = {
    eleven: 11 * this.secondsInAMinute * this.milliSecondsInASecond,
    ten: 10 * this.secondsInAMinute * this.milliSecondsInASecond,
    six: 6 * this.secondsInAMinute * this.milliSecondsInASecond,
    five: 5 * this.secondsInAMinute * this.milliSecondsInASecond,
    one: 1 * this.secondsInAMinute * this.milliSecondsInASecond,
  };

  myTime = {
    days: 0,
    hours: 0,
    minutes: 1000, // greater than 10 for validation purpose (see HTML component)
    seconds: 0,
  };

  timeDifference!: number;

  private countdownSubscription!: Subscription;
  private sessionSubscription!: Subscription;

  constructor(
    private avatarService: AvatarService,
    private profileService: ProfileService,
    private router: Router,
    private toastr: ToastrService,
    public authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.profile = this.profileService.profile;
    this.notifySessionStatus();
    this.countdownSubscription = interval(1000).subscribe(() =>
      this.verifyCountDown()
    );
    this.sessionSubscription = interval(60000).subscribe(() =>
      this.notifySessionStatus()
    );
    this.user = this.authService.user;
  }

  ngOnDestroy(): void {
    this.countdownSubscription.unsubscribe();
    this.sessionSubscription.unsubscribe();
  }

  logout(): void {
    this.toastr.info(`${this.authService.user.firstName}`, 'Good Bye!', {
      progressBar: true,
    });
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  askForRefreshToken(): void {
    Swal.fire({
      title: `${this.authService.user.firstName}`,
      text: `¿Deseas extender el tiempo de sesión?`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
      timer: 30000,
      timerProgressBar: true,
    }).then((result) => {
      if (result.isConfirmed) {
        this.refreshToken();
      }
    });
  }

  private refreshToken(): void {
    this.authService.getNewToken(this.user).subscribe({
      next: (res) =>
        this.toastr.info(`Your session has been extended`, 'Done!', {
          progressBar: true,
        }),
      error: (err) =>
        this.toastr.warning(`Try again`, 'Error!', { progressBar: true }),
    });
  }

  private verifyCountDown(): void {
    this.getTimeDifference();

    if (this.timeDifference <= 0) {
      Swal.fire('Good Bye!', `The session has expired`, 'info');
      this.logout();
    }
  }

  private getTimeDifference(): void {
    this.timeDifference =
      this.authService.getExpirationDate().getTime() - new Date().getTime();
    this.allocateTimeUnits();
  }

  private allocateTimeUnits(): void {
    this.myTime = {
      days: Math.floor(
        this.timeDifference /
          (this.milliSecondsInASecond *
            this.minutesInAnHour *
            this.secondsInAMinute *
            this.hoursInADay)
      ),
      hours: Math.floor(
        (this.timeDifference /
          (this.milliSecondsInASecond *
            this.minutesInAnHour *
            this.secondsInAMinute)) %
          this.hoursInADay
      ),
      minutes: Math.floor(
        (this.timeDifference /
          this.milliSecondsInASecond /
          this.minutesInAnHour) %
          this.secondsInAMinute
      ),
      seconds: Math.floor(
        (this.timeDifference / this.milliSecondsInASecond) %
          this.secondsInAMinute
      ),
    };
  }

  private notifySessionStatus(): void {
    this.getTimeDifference();

    if (
      (this.timeDifference < this.minutesInMilliseconds.eleven &&
        this.timeDifference >= this.minutesInMilliseconds.ten) ||
      (this.timeDifference < this.minutesInMilliseconds.six &&
        this.timeDifference >= this.minutesInMilliseconds.five)
    ) {
      Swal.fire({
        title: `${this.myTime.minutes} minutos`,
        text: `${this.authService.user.firstName}, tu sesión está por terminar ¿Desea extender el tiempo de sesión?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'No',
        timer: 30000,
        timerProgressBar: true,
      }).then((result) => {
        if (result.isConfirmed) {
          this.refreshToken();
        }
      });
    }
  }

  avatar(): string {
    const text =
      this.authService.user.firstName[0] + ' ' + this.authService.user.lastName;
    return this.avatarService.generateAvatar(text, '#e9ecef', '#222e3c');
  }
}
