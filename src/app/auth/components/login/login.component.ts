import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../services/auth.service';
import { Config } from 'src/app/config/config';
import { User } from 'src/app/shared/models/user';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  title = Config.APP.name;
  user: User;
  loginForm!: UntypedFormGroup;

  constructor(
    private fb: UntypedFormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private http: HttpClient
  ) {
    this.user = new User();
    this.createForm();
  }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.toastr.info(
        `${this.authService.user.firstName} is already authenticated`,
        'User authenticated',
        {
          progressBar: true,
        }
      );
      this.router.navigate(['/']);
    }
  }

  createForm(): void {
    this.loginForm = this.fb.group({
      user: [
        this.user.username,
        [Validators.required, Validators.minLength(4)],
      ],
      password: [this.user.password, Validators.required],
    });

    this.loginForm.valueChanges.subscribe((val) => {
      this.user = val;
    });
  }

  entrar(): void {
    console.log('ENTRAR');
    console.log('=>', this.loginForm.getRawValue());
    this.http
      .post('http://localhost:3301/auth/login', this.loginForm.getRawValue(), {
        withCredentials: true,
      })
      .subscribe((resp) => console.log(resp));
  }

  login(): void {
    console.log('LOGIN');
    this.authService.login(this.user).subscribe({
      next: (res) => {
        this.toastr.success(`${this.authService.user.firstName}`, 'Welcome!', {
          progressBar: true,
        });

        /**
         * TODO: Before to use router.navigate instead of window.location.href, we have to fix the home js loading because it isn't working well
         * this.router.navigate(['/']);
         */
        window.location.href = '/';
      },
      error: (err) => {
        console.log(err);
        if (err.status === 400) {
          this.toastr.warning('Try again', 'Wrong credentials', {
            progressBar: true,
          });
        } else if (err.status >= 500) {
          this.toastr.error('Try again', 'Server Error', {
            progressBar: true,
          });
        } else {
          this.toastr.error('Try again', 'Error', {
            progressBar: true,
          });
        }
      },
    });
  }
}
