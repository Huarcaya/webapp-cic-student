import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.logout();
  }

  logout(): void {
    if (this.authService.isAuthenticated()) {
      this.toastr.info(`${this.authService.user.firstName}`, 'Good Bye!', {
        progressBar: true,
      });
    }
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
