import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Router } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((e, c) => {
        // tslint:disable-next-line: triple-equals
        if (e.status == 401) {
          if (this.authService.isAuthenticated()) {
            this.authService.logout();
          }
          this.router.navigate(['/login']);
        }

        // tslint:disable-next-line: triple-equals
        if (e.status == 403) {
          Swal.fire(
            'Access denied',
            `${this.authService.user.firstName} don't have access to access this resource!`,
            'warning'
          );
          this.router.navigate(['/']);
        }

        return throwError(() => new Error(e));
      })
    );
  }
}
