import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }

    /*const role = route.data.role as string;

      if (this.authService.hasRole(role)) {
        return true;
      }*/

    let hasRole = false;
    const roles = route.data.role as string[];
    roles.forEach((role) => {
      if (this.authService.hasRole(role)) {
        hasRole = true;
      }
    });
    if (hasRole) {
      return true;
    }

    this.toastr.error(
      `${this.authService.user.username} no tiene acceso para acceder a esta sección`,
      'Acceso rectringido',
      {
        progressBar: true,
      }
    );

    return false;
  }
}
