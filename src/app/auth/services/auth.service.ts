import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Config } from 'src/app/config/config';
import { User } from 'src/app/shared/models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _user!: User | null;
  private _token!: string;
  private _refreshToken!: string;
  private apiUrl = Config.API_URL;
  private readonly loginEndpoint = `${this.apiUrl}auth/login`;
  private readonly refreshTokenEndpoint = `${this.apiUrl}auth/refresh`;
  private readonly credentials = btoa(Config.CREDENTIALS);
  private readonly httpHeaders = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: 'Basic ' + this.credentials,
  });

  constructor(private http: HttpClient) {}

  public get user(): User {
    const localStorageUser = localStorage.getItem('user');

    if (this._user != null) {
      return this._user;
    } else if (this._user == null && localStorageUser != null) {
      this._user = JSON.parse(localStorageUser) as User;
      return this._user;
    }

    return new User();
  }

  public get token(): string | null {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && localStorage.getItem('user') != null) {
      this._token = localStorage.getItem('token') || '';
      return this._token;
    }
    return null;
  }

  public get refreshToken(): string {
    if (this._refreshToken != null) {
      return this._refreshToken;
    } else if (
      this._refreshToken == null &&
      localStorage.getItem('user') != null
    ) {
      this._refreshToken = localStorage.getItem('refreshToken') || '';
      return this._refreshToken;
    } else {
      return '';
    }
  }

  login(user: User): Observable<any> {
    user.grant_type = 'password';

    return this.http
      .post<any>(this.loginEndpoint, user, {
        withCredentials: true,
      })
      .pipe(
        tap((res) => {
          this.saveUser(res.access_token);
          this.saveToken(res.access_token);
          this.saveRefreshToken(res.refresh_token);
        })
      );
  }

  getNewToken(user: User): Observable<any> {
    user.grant_type = 'refresh_token';

    return this.http
      .post<any>(this.refreshTokenEndpoint, user, {
        headers: this.httpHeaders,
      })
      .pipe(
        tap((res) => {
          this.saveUser(res.access_token);
          this.saveToken(res.access_token);
          this.saveRefreshToken(res.refresh_token);
        })
      );
  }

  saveUser(accessToken: string): void {
    const payload = this.getTokenInfo(accessToken);
    this._user = new User();
    this._user.username = payload.username;
    this._user.firstName = payload.firstName;
    this._user.lastName = payload.lastName;
    this._user.pidm = payload.pidm;
    this._user.roles = payload.authorities;
    localStorage.setItem('user', JSON.stringify(this._user));
  }

  saveToken(accessToken: string): void {
    this._token = accessToken;
    localStorage.setItem('token', accessToken);
  }

  saveRefreshToken(refreshToken: string): void {
    this._refreshToken = refreshToken;
    localStorage.setItem('refreshToken', refreshToken);
  }

  getTokenInfo(accessToken: string): any {
    if (accessToken) {
      return JSON.parse(atob(accessToken.split('.')[1]));
    }
    return null;
  }

  isAuthenticated(): boolean {
    const token = this.token ?? '';
    const payload = this.getTokenInfo(token);

    return (
      payload != null &&
      payload.username &&
      payload.username.length > 0 &&
      this.hasActiveToken()
    );
  }

  hasActiveToken(): boolean {
    return this.getExpirationDate().getTime() - new Date().getTime() > 0;
  }

  hasRole(role: string): boolean {
    return this.user.roles.includes(role);
  }

  getExpirationDate(): Date {
    const token = this.token ?? '';
    const payload = this.getTokenInfo(token);
    if (payload != null && payload.exp) {
      return new Date(payload.exp * 1000);
    }
    return new Date(payload.exp);
  }

  logout(): void {
    this._token = '';
    this._refreshToken = '';
    this._user = null;

    // It's better to clean all localStorage because
    // the app stores different kinds of information
    // and it's awful to clean name by name
    localStorage.clear();
  }
}
