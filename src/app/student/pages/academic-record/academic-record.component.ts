import { Component, OnInit } from '@angular/core';

import { GradesService } from '../../services/grades.service';
import { Language } from 'src/app/shared/interfaces/language.interface';
import { ProfileService } from 'src/app/user/services/profile.service';

@Component({
  selector: 'app-academic-record',
  templateUrl: './academic-record.component.html',
  styleUrls: ['./academic-record.component.scss'],
})
export class AcademicRecordComponent implements OnInit {
  contentLoaded: boolean = false;
  grades: any[] = [];
  profile!: Language;

  constructor(
    private gradesService: GradesService,
    private profileService: ProfileService
  ) {}

  ngOnInit(): void {
    this.profile = this.profileService.profile;
    this.getLanguageGrades();
  }

  getLanguageGrades(): void {
    this.gradesService.getLanguageGrades(this.profile.id).subscribe({
      next: (grades) => (this.grades = grades),
      error: (err) => (this.contentLoaded = true),
      complete: () => (this.contentLoaded = true),
    });
  }
}
