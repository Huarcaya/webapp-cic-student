import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { AcademicRecordComponent } from './pages/academic-record/academic-record.component';
import { AttendanceComponent } from './pages/attendance/attendance.component';
import { StudentRoutingModule } from './student-routing.module';

@NgModule({
  declarations: [AcademicRecordComponent, AttendanceComponent],
  imports: [CommonModule, StudentRoutingModule, NgxSkeletonLoaderModule],
})
export class StudentModule {}
