import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { ProfileComponent } from './pages/profile/profile.component';
import { UserComponent } from './components/user/user.component';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  declarations: [UserComponent, ProfileComponent],
  imports: [CommonModule, UserRoutingModule, NgxSkeletonLoaderModule],
})
export class UserModule {}
