import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Config } from 'src/app/config/config';
import { Language } from 'src/app/shared/interfaces/language.interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseEndpoint = `${Config.API_URL}`;

  constructor(private http: HttpClient) {}

  getLanguages(): Observable<Language[]> {
    const url = `${this.baseEndpoint}languages`;
    return this.http.get<Language[]>(url);
  }
}
