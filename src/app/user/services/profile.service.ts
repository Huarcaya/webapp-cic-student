import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Language } from 'src/app/shared/interfaces/language.interface';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  private _profile!: Language;

  constructor(private router: Router) {}

  get profile(): Language {
    const localProfile: string = localStorage.getItem('profile') ?? '';

    if (!localProfile) {
      this.router.navigate(['/profile']);
    }

    if (this._profile) {
      return this._profile;
    }

    this._profile = JSON.parse(localProfile) as Language;

    return this._profile;
  }

  set profile(language: Language) {
    localStorage.setItem('profile', JSON.stringify(language));
    this._profile = language;
  }

  cleanProfile(): void {
    localStorage.removeItem('profile');
  }

  profileSelected(): boolean {
    if (this.profile?.id) {
      return true;
    }
    return false;
  }
}
