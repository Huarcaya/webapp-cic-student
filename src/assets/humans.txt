/* TEAM */
    Web Developer: Gustavo Huarcaya
    Contact: diavolo [at] gahd.net
    Site: https://gahd.net
    Location: Ica, Peru


/* THANKS */
    Yhuvali, Daniel, and Amy.


/* SITE */
    Standards: HTML5, CSS3, W3C (as much as possible)
    Core: Angular
    Components: Bootstrap
    Software: VSCode
